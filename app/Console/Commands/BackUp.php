<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BackUp extends Command
{
    const BACKUP_PATH = 'database/latest.sql';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $command = "mysqldump --user=" . env('DB_USERNAME') ." --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  | gzip > " . base_path(self::BACKUP_PATH);
        $returnVar = NULL;
        $output  = NULL;
        exec($command, $output, $returnVar);
        $this->mail();
        //return response()->json(['status' => 'success', 'download-link' => route('backup.download')], 200);
    }

    public function mail(){
        $siteName = "MY ISD";
        $to = 'sehsahdeveloper@gmail.com'; 
        $from = 'mahmoudsehsah36@gmail.com'; 
        $fromName = 'Db Backup'.$siteName; 
        $subject = 'Backup '.$siteName.' Database';  
        $file = base_path(self::BACKUP_PATH); 
        $htmlContent = ' <h3>Backup "'.$siteName.'" Database time : </h3> '.date("Y-m-d h:i:sa");;
        $headers = "From: $fromName"." <".$from.">"; 
        $semi_rand = md5(time());  
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";  
        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 
        $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" . 
        "Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";  
        if(!empty($file) > 0){ 
            if(is_file($file)){ 
                $message .= "--{$mime_boundary}\n"; 
                $fp =    @fopen($file,"rb"); 
                $data =  @fread($fp,filesize($file)); 
         
                @fclose($fp); 
                $data = chunk_split(base64_encode($data)); 
                $message .= "Content-Type: application/octet-stream; name=\"".basename($file)."\"\n" .  
                "Content-Description: ".basename($file)."\n" . 
                "Content-Disposition: attachment;\n" . " filename=\"".basename($file)."\"; size=".filesize($file).";\n" .  
                "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n"; 
            } 
        } 
        $message .= "--{$mime_boundary}--"; 
        $returnpath = "-f" . $from; 
        $mail = @mail($to, $subject, $message, $headers, $returnpath);  
    }    
}
