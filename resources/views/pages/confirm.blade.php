@extends('layouts.app')
@section('pageClass', 'athleticpg inner-page')
@section('title', 'login')
@section('content')
<!-- About laliga -->
<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">
        <div class="row align-items-center">
            <!-- <div class="col-lg-2 order-lg-last">
                <picture class="logo-icon">
                    <img src="/assets-web/images/logos/isdathletics.svg" alt="">
                </picture>
            </div> -->
            <div class="col-lg-10">
                <h2 class="maintitle">
                    <span class="fc-football">ISD Email Verification Code</span>
                </h2>
            </div>
        </div>
        <hr class="divider">
        <div class="content-section mb-40">
            <div class="row">
                <section class="col-xl-5 offset-xl-4 col-lg-6 offset-lg-3">
                    <div class="box --registration-box">
                        <h4 class="fc-white mb-40">Enter Email Verification Code before Continuing</h4>
                        <form class="default-form --registration-form">

                            <div class="control-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                {!! Form::text('code', null, ['class'=>'form-field','required' => 'required',
                                'placeholder'=>'Enter Email Verification Code','id'=>'code','autocomplete'=>'off']) !!}
                                @if ($errors->has('player_nam'))
                                <span class="form-error">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="control-group">
                                <button type="button" id="click" class="btn --btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                            <p class="fc-football">Check your email for verification code to verify your email address
                            </p>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
    
    $('#click').click(function(){
    
    var id={{$user}};
    var code=$("#code").val();
    
    $.ajax({
    type: 'POST',
    url: 'https://bookings.isddubai.com/api/v1/users/register/confirm',
    data: {id:id,code:code},
    dataType: 'json',
    success: function (data) {
    console.log(data);
    var status = data.status;
    var errormsg = data.msg;
    if(status==200)
    {
    window.location.href = "{{ route('client.bookings')}}";
    }
    else if(status==500){
    alert(errormsg);
    }
    },
    error: function (data) {
    console.log(data);
    }
    });
    });
    });
    </script>
</section>
<!-- Event snippet for Submit lead form - Popup conversion page -->
@endsection