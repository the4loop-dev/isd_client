@extends('layouts.user')
@section('content')

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">REVIEW BOOKING</h2>

            <p class="maindesc --big">
                Please review your selection and confirm by clicking on the button to proceed to payment
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    <div class="table-responsive">

                        <table class="table --schedule-table --review-table athletics text-left" id="all-bookings" width="100%" cellspacing="0">
                            <tr>
                                <th>Booking #</th>
                                <th>Date/ Time</th>
                                <th>Pitch</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>

                            @foreach($booking_details as $key=> $detail)

                            <tr>
                                <td>
                                    {{$detail->id}}<br>
                                </td>
                                
                                <td>
                                    {{$detail->date}}<br>
                                    {{$detail->from}}<br>{{$detail->to}}
                                </td>

                                <td>
                                    {{$detail->pitch}}
                                </td>
                                <td>
                                   {{$detail->size}}
                                </td>
                                <td>
                                    {{$detail->price}}
                                </td>
                                <td>
                                    {{$detail->dicsount}}<br>
                                </td>
                                <td>
                                    
                                    @if($detail->status == 0)
                                        Unpaid         
                                    @else
                                        Paid        
                                    @endif
                                
                                </td>
                                <td>
                                    {{$detail->total}}<br>
                                </td>
                                <td>
                                    <a href="{{ url('client/booking-detail/'.$detail->id) }}" class="badge --success d-block mb-6" target="_blank"> 
                                        View
                                    </a>
                                    <a href="{{ url('../export/invoice/'.$detail->id) }}" class="badge --info d-block mb-6" target="_blank">
                                        Invoice
                                    </a>
                                    <a href="{{ url('../export/receipt/'.$detail->id) }}" class="badge --warning d-block" target="_blank">
                                        Receipt
                                    </a>
                                </td>
                            </tr>

                            @endforeach
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
    

<!-- /.container-fluid -->
@endsection