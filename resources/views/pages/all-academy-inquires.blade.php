@extends('layouts.user')
@section('content')

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle"> Academy Inquires</h2>

            <p class="maindesc --big">
                Please review your selection and confirm by clicking on the button to proceed to payment
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    <div class="table-responsive">

                        <table class="table --schedule-table --review-table athletics text-left" id="all-bookings" width="100%" cellspacing="0">
                            <tr>
                                <th>Inquires #</th>
                                <th>Start Date</th>
                                <th>Age Category</th>
                                <th>Package Name</th>
                                <th>Class</th>
                                <th>Days</th> 
                                <th>Fees</th>
                            </tr>

                           @foreach($inquires_details as $key=> $detail)

                            <tr>
                                <td>
                                    {{$detail->id}}<br>
                                </td>
                                
                                <td>
                                    {{ date('d-m-Y',  strtotime($detail->start_date))}}
                                </td>

                                <td>
                                    {{$detail->age_title}}
                                </td>
                                <td>
                                   {{$detail->package_name}}
                                </td>
                                <td>
                                    {{$detail->class_title}}
                                </td>
                                <td> @php $s = str_replace( array( '[','"', ',', ']', '\''), ' ', $detail->num_days); @endphp
                                      {{ $s }}
                                </td>
                                <td>
                                    AED {{ number_format((float)$detail->subscription_fees, 2, '.', '') }}<br>
                                </td>
                            </tr>

                            @endforeach 
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
    

<!-- /.container-fluid -->
@endsection