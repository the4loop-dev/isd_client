@extends('layouts.user')
@section('content')

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">Account Details</h2>

        
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">
                    
                   

                    <div class="table-responsive">

                        <table class="table --schedule-table --review-table athletics text-left" id="all-bookings" width="100%" cellspacing="0">
                            
                            <tr>
                                <th>Card Number</th>
                                <th>Action</th>
                            </tr>
                             @foreach($cards as $key=> $card)

                            <tr>
                                <td> {{$card->card_number}} </td>
                            
                                <td>
                    
 {{-- {!! Form::open(['method' => 'post', 'id' => 'deleteForm']) !!} --}}

                                 <form method="post" action="{{ route('client.payment-card-delete') }}">
                                     
                                     @csrf
                                     
                          <input type="hidden" name="token_name" value="{{$card->token_name}}" >
                          <input type="hidden" name="randomnum" value="{{$card->randomnum}}" >
                          <input type="hidden" name="booking_id" value="{{$card->booking_id}}" >
                          <input type="hidden" name="order_description" value="{{$card->order_description}}" >
                          <input type="hidden" name="amount" value="{{$card->amount}}" >
                             <input type="hidden" name="client_id" value="{{$card->user_id}}" >
                             
                             
                                            <button type="submit" class="deleteItem badge --danger mb-6">Remove</button>
                                 
                             
                    </form>


{{-- {!! Form::close() !!} --}}
    

 
                                </td>
                            </tr>

                            @endforeach
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
</section>

<!-- /.container-fluid -->


<script>
    
    
        //  var deleteResourceUrl = '{{ url()->current() }}';

        // $(document).ready(function() {
        //     $(document).on('click', '.deleteItem', function(e) {
        //         e.preventDefault();
        //         var deleteId = $(this).data('deleteId');
        //         $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
        //         if (confirm('Are you sure?')) {
        //             $('#deleteForm').submit();
        //         }
        //     })
    
        // });
    
    
</script>


@endsection