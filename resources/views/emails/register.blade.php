<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Verification</title>

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;600;700&display=swap" rel="stylesheet">

</head>
<body style="background: #ffffff; margin: 0; padding: 0; font-family: Arial, sans-serif; font-size: 18px; line-height: 1.6; color: #3e2b64;">
	<div style="margin: 0 auto; padding: 0px; width: 600px; max-width: 600px;">
		<table cellpadding="0" style="width: 600px; background: #ffffff; border-collapse: collapse; border-spacing: 0; max-width: 600px; margin: 0 auto;">
			<tbody>

				<tr>
					<td colspan="2">
						<a href="https://isddubai.com/" target="_blank" style="display: block;">
							<img src="http://www.isddubai.com/emails/app-emails/verification/images/header.png" alt="ISD Dubai" style="display: block; margin: 0 auto;">
						</a>
					</td>
				</tr>

				<tr>
					<td colspan="2" style="padding: 20px 0;">

						<p style="margin: 0 60px 20px; line-height: 1.4; font-size: 16px; font-family: Arial, sans-serif; color: #3e2b64;">

							Thank you for your registration. <br><br>

							<strong style="font-size: 18px;">You have received a verification email.</strong> <br>
							Please verify your email to proceed.

						</p>

						<p style="margin: 0 60px 40px; line-height: 1.4; font-size: 16px; font-family: Arial, sans-serif; color: #3e2b64;">

							Code : {{$details['activation_code']}}

						</p>

						<p style="margin: 0 60px 0px; line-height: 1.4; font-size: 20px; font-family: 'Oswald', Arial, sans-serif; font-weight: 700; color: #3e2b64; text-transform: uppercase;"> 

							isddubai.com

						</p>

					</td>
				</tr>

				<tr>

					<td>

						<img src="http://www.isddubai.com/emails/app-emails/verification/images/footer.png" alt="ISD Dubai" style="display: block; margin: 0 auto;">

					</td>
				</tr>

			</tbody>
		</table>
	</div>	
</body>
</html>