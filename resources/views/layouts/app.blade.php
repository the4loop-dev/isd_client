<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title> | ISD Dubai</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://isddubai.com/assets-web/css/style.css"/>
    
    <!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    .bg-init,
    .animsition-overlay-slide {
    display: block;
    background-image: url('/public/assets-web/images/logos/isddubai-white.svg');
    background-size: 10%;
    background-position: center;
    background-repeat: no-repeat;
    }
    </style>
    
<style>
.primary-navigation .header-navigation{
    right: 10px !important;
}


.social-links .icon {
    height: 40px;
    width: 40px;
    display: grid;
    align-items: center;
    font-size: 15px;
    justify-content: center;
    text-decoration: none;
    margin: 10px 5px;
    border-radius: 50%;
}

.social-links .icon.fa:hover {
    opacity: 0.7;
}

.social-links .icon.fa-facebook {
    background: #3B5998;
    color: white;
}

.social-links .icon.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.social-links .icon.fa-linkedin {
  background: #007bb5;
  color: white;
}

.social-links .icon.fa-youtube {
  background: #bb0000;
  color: white;
}

.social-links .icon.fa-instagram {
  background: #E1306C;
  color: white;
}

.social-links .icon.fa-pinterest {
  background: #cb2027;
  color: white;
}

.social-links .icon.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.social-links .icon.fa-skype {
  background: #00aff0;
  color: white;
}

.social-links .icon.fa-android {
  background: #a4c639;
  color: white;
}

.social-links .icon.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.social-links .icon.fa-vimeo {
  background: #45bbff;
  color: white;
}

.social-links .icon.fa-tumblr {
  background: #2c4762;
  color: white;
}

.social-links .icon.fa-vine {
  background: #00b489;
  color: white;
}

.social-links .icon.fa-foursquare {
  background: #45bbff;
  color: white;
}

.social-links .icon.fa-stumbleupon {
  background: #eb4924;
  color: white;
}


</style>
    
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body class="bg-init innerpage">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    

    <main class="app-container animsition-overlay">

        @include('includes.navigation')

        @yield('content')

        @include('includes.footer')

    </main>

   
    <!-- <div class="overlay-bg"></div> -->
    <script type="text/javascript" src="{{ asset('public/assets-web/js/functions.js')}}"></script>
    <script type="text/javascript" src="{{ asset('public/assets-web/js/script.js')}}"></script>
    <script>
    $(window).load(function() {
    // Animate loader off screen
    $(".pre-loading").fadeOut("slow");;
    });
    $(document).ready(function() {
    $('.animsition-overlay').animsition({
    inClass: 'overlay-slide-in-top',
    outClass: 'overlay-slide-out-bottom',
    overlay : true,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body'
    })
    .one('animsition.inStart',function(){
    $('body').removeClass('bg-init');
    })
    .one('animsition.inEnd',function(){
    });
    });
    </script>
</body>
</html>