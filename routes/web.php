<?php



use Illuminate\Support\Facades\Route;



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Route::get('/', function () {

     return redirect('admin');

});





//Auth::routes();

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route::get('/user-login', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('/user-login', 'Auth\LoginController@Login')->name('login');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/{user}/email-verification', 'HomeController@emailVerification')->name('user.email.verification');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/delete/{id}', 'TimesController@DeleteTime');

Route::get('/check/courts/{id}', 'CourtsController@CheckTime');

Route::get('/check/products/{id}', 'BookingsController@CheckProduct');

Route::post('/check/sport', 'CourtsController@CheckSport');

Route::post('/check/book', 'BookingsController@store');

Route::post('/add/pay', 'BookingsController@AddPay');

Route::get('/export/invoice/{id}', 'BookingsController@ExportInvoice');

Route::get('/export/receipt/{id}', 'BookingsController@ExportReceipt');

Route::get('/order/product/{id}', 'BookingsController@DeleteOrderProduct');

Route::get('/backup','AdminController@backup');

Route::get('/active','BookingsController@active');


Route::name('client.')->prefix('client/')->middleware(['user'])->group(function(){

    Route::get('/bookings', 'HomeController@bookings')->name('bookings');
    
    Route::get('/all-bookings', 'HomeController@all_bookings')->name('allbookings');
    
    Route::get('/booking-detail/{id}', 'HomeController@bookings_detail')->name('bookingdetail');
    
    Route::post('check/courts', 'HomeController@checkSport')->name('check.courts');
    
    Route::post('store/bookings', 'HomeController@addBooking')->name('bookings.store');
    
    Route::post('check/tokens', 'HomeController@token_test')->name('bookings.token');
    
    Route::post('/booking', 'HomeController@resetForm')->name('reset.form');
    
    Route::get('/{court}/products/','HomeController@products')->name('bookings.products');
    
    Route::get('/manage-card', 'ProfileManageController@card_manage')->name('managecard');

    Route::get('/all-academy-inquires', 'HomeController@all_academy_inquires')->name('allacademyinquires');

    Route::get('/payment-cards', 'HomeController@allCards')->name('payment-cards');

    Route::post('/payment-cards', 'HomeController@cardDelete')->name('payment-card-delete');

});

Route::post('/client/response/bookings', 'HomeController@merchant_response');

Route::post('/client/response', 'HomeController@update_token')->name('post-token');

Route::get('/client/response/merchantDetails', 'HomeController@getMerchantDetails')->name('getMerchantDetails');

Route::get('/client/response/getMerchantToken', 'HomeController@getMerchantToken')->name('getMerchantToken');

// wallet process
Route::post('/client/process/processWallet', 'HomeController@processWallet')->name('processWallet');

Route::post('/client/process/processWalletSuccess', 'HomeController@processWalletSuccess')->name('processWalletSuccess');

Route::get('/client/booking/{booking_id}/thankyou/{booking_amount}', 'HomeController@thankyou')->name('wallet.booking.thankyou');


// Route::post('/client/response/bookings',function(){
//   return response()->json();
// });


// Route::get('/active', function () {

//     $update=  DB::table('users')->where('id', $_GET['id'])->where('email', $_GET['email'])->where('activation_code', $_GET['code'])->update(['status' => 1]);

//     if($update){

//         //Mail::to($_GET['email'])->send(new \App\Mail\Welcom());        

//         return "your Account is Active Now";

//     }else{

//      return "Error Active You Account";

//     }

// });



Route::group(['prefix' => 'admin'], function () {

    Voyager::routes();

    Route::get('/reports/booking', 'ReportsController@Booking');

    Route::get('/reports/schedule', 'ReportsController@Schedule');

    Route::get('/reports/finance', 'ReportsController@Finance');

    Route::get('/reports/academy', 'ReportsController@Academy');





});


Route::get('/academy', 'HomeController@loadview')->name('academy');
Route::post('/academy/availableClasses', 'HomeController@availableClasses')->name('availableClasses');
Route::post('/academy/submitAcademy', 'HomeController@submitAcademy')->name('submitAcademy');

Route::get('/getCategory', 'HomeController@getCategory')->name('getCategory');


Route::post('/getPrice', 'HomeController@getPrice')->name('getPrice');


// Without api
Route::post('/user/show', 'HomeController@UserDetails')->name('user.show');
Route::get('/academy/terms/', 'HomeController@Terms')->name('academy.terms');
Route::post('/academy/packages/check', 'HomeController@MyPackages')->name('academy.packages.check');

// Calculate Package
Route::post('/academy/calculate/amount', 'HomeController@CalculateAmount')->name('academy.calculate.amount');

// Subscription add  
Route::post('/academy/subscriptions/add', 'HomeController@StoreSubscriptions')->name('academy.subscriptions.add');


